int rootScore;
MOVE rootMove;

// time-control variables
int startTime;
int noNewIterationLimit;
int noNewMoveLimit;
int neverExceedLimit;

#define MARGIN 25

int TimeUsed()
{
  return ReadClock() - startTime;
}

void RootSearch()
{
  startTime = ReadClock();
  GenerateMoves();
  for(depth=1; depth<=maxDepth && TimeUsed()<noNewIterationLimit; depth++) { // iterative deepening loop
    bestScore = -INFINITY;
    for(ALL_MOVES) {
      MakeMove();
      score = -Search(...);
      UnMake();
      if(abortFlag) return;
      // handle score, etc.
      ...
      if(TimeUsed() > noNewMoveLimit && bestScore > rootScore - MARGIN) break;
    }
    // end of iteration
    ...
    rootMove = bestMove;
    rootScore = bestScore;
  } 
}

#define GUESSEDLENGTH 40

void SetTimeLimits(int msecLeft, int st, int tc, int inc, int mps, int moveNr)
{
  int movesToGo = mps - moveNr/2;

  if(st > 0) movesToGo = 1;                    // in maximum-time-per-move mode, the time left is always for one move
  else if(mps == 0) movesToGo = GUESSEDLENGTH; // in sudden-death, we have to guess how many moves we still must do
  else while(movesToGo <= 0) movesToGo += mps; // calculate moves before next time control

  msecLeft -= 16; // keep absolute safety margin of 16 msec

  neverExceedLimit    =  10*msecLeft / (movesToGo + 9);
  noNewMoveLimit      = 1.5*msecLeft / (movesToGo + 0.5);
  noNewIterationLimit = 0.5*msecLeft / movesToGo;
}