
/* http://www.open-aurec.com/wbforum/viewtopic.php?p=198888#p198888 */

#include <stdio.h>
#include <string.h>

void out(FILE *f, char *s)
{
  printf(s); // to GUI
  fprintf(f, "out: %s", s); // on log file
  fflush(f);
}

int main(int argc, char **argv)
{
  FILE *f = fopen("log.txt", "w");
  int j;

  for(j=0; j<argc; j++) fprintf(f, "%d. %s\n", j, argv[j]), fflush(f); // command line to log file

  out(f, "max threads set to 2\n");
  out(f, "XQMS 3.26_10_19. By  ¢»Í¥├¶íóı┼├÷.\n");

  while(1) {
   int i=0, x;
   char line[1000], command[100];

   fflush(stdout);
   while((line[i] = x = getchar()) != EOF && line[i] != '\n') i++;
   line[++i] = 0;
   fprintf(f, "in:  %s", line); fflush(f);
   sscanf(line, "%s", command);
   if(!strcmp(command, "quit")) return 1;
   else if(!strcmp(command, "uci")) {
      out(f, "id name XQMS 3.26_10_19\n");
      out(f, "id author ¢»Í¥├¶íóı┼├÷.\n\n");
      out(f, "uciok\n\n");
      out(f, "XQMS Use uci[ucci] protocol\n");
   }
   else if(!strcmp(command, "isready")) out(f, "readyok\n");
  }
}
