int Search(...)
{
  GenerateMoves();
  for(ALL_MOVES) {
    MakeMove();
    score = -Search(...);
    UnMake();
    if(abortFlag) return 0;
    // handle score, etc.
    ...
  }
  return bestScore;
}