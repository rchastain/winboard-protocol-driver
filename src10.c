#define OPPONENT(x) WHITE + BLACK - (x)

main()
{
  int stm=WHITE;                           // side to move
  int engineSide=NONE;                     // side played by engine
  int maxDepth;                            // used by search
  MOVE move, ponderMove;
  int i, score;

  while(1) { // infinite loop

    fflush(stdout); // make sure everything is printed before we do something that might take time
    inBuf[0] = 0; // empty input buffer, so we can detect if something appears in it

    pondering = (ponder && engineSide == OPPONENT(stm) && moveNr != 0); // calculate if we are expected to ponder
    if(engineSide == stm || pondering && ponderMove != INVALID) {
      MOVE newPonderMove; int oldStm = stm;
      if(pondering) { // we are apparently pondering on a speculative move
        sprintf(ponderMoveText, "%s\n", MoveToText(ponderMove)); // remember ponder move as text (for hit detection)
        stm = MakeMove(stm, ponderMove);
        SetTimeLimits(INFINITE); // make sure we will never terminate because of time
      } else SetTimeLimits(10*timeLeft);
      score = SearchBestMove(stm, &move, &newPonderMove);
      if(pondering) { // pondering was aborted because of miss or other command
        UnMake(ponderMove); stm = oldStm;
        pondering = FALSE;
      } else if(move == INVALID) {  // game apparently ended
        engineSide = NONE;          // so stop playing
        PrintResult(stm, score);
      } else {
        stm = MakeMove(stm, move);  // assumes MakeMove returns new side to move
        gameMove[moveNr++] = move;  // remember game
        printf("move %s\n", MoveToText(move));
        ponderMove = newPonderMove;
        continue; // start pondering (if needed)
      }
    } else if(engineSide == ANALYZE || pondering) { // this catches pondering when we have no move
      MOVE dummy;
      pondering = TRUE;        // in case we must analyze
      ponderMoveText[0] = 0;   // make sure we will never detect a ponder hit
      SetTimeLimits(INFINITE); // make sure we will never terminate because of time
      SearchBestMove(stm, &dummy, &dummy);
      pondering = FALSE;
    }

    fflush(stdout); // make sure everything is printed before we do something that might take time
    // the previous calls to SeachBestMove() could have left a command that interrupted it in inBuf !
    if(inBuf[0] == 0) PeekAtInput(1); // handles time, otim

    // recognize the command,and execute it
    if(!strcmp(command, "quit"))    { break; } // breaks out of infinite loop
    if(!strcmp(command, "force"))   { engineSide = NONE;    continue; }
    if(!strcmp(command, "analyze")) { engineSide = ANALYZE; continue; }
    if(!strcmp(command, "exit"))    { engineSide = NONE;    continue; }
    ... // processing of other commands that do not alter the position (or side to move)
    ponderMove = INVALID;
    ... // processing of commands that do alter the position (new, setboard, usermove, undo, remove, white, black)
  }
}