#define GUESSEDLENGTH 40

int mps, timeControl, inc, timePerMove;  // time-control parameters
int neverExceedLimit, noNewMoveLimit, noNewIterationLimit; // used by Search()
int timeLeft;
char inBuf[80], command[80], ponderMoveText[20];
char abortFlag, pondering;

void SetTimeLimits(int msecLeft)
{
  int movesToGo = mps - moveNr/2;

  if(st > 0) movesToGo = 1;                    // in maximum-time-per-move mode, the time left is always for one move
  else if(mps == 0) movesToGo = GUESSEDLENGTH; // in sudden-death, we have to guess how many moves we still must do
  else while(movesToGo <= 0) movesToGo += mps; // calculate moves before next time control

  msecLeft -= 16; // keep absolute safety margin of 16 msec

  neverExceedLimit    =  10*msecLeft / (movesToGo + 9);
  noNewMoveLimit      = 1.5*msecLeft / (movesToGo + 0.5);
  noNewIterationLimit = 0.5*msecLeft / movesToGo;
}

int PeekAtInput(int root)
{
  while(1) {
    ReadLine(inBuf);
    sscanf(inBuf, "%s", command);
    if(!strcmp(command, "otim"))    { continue; } // do not resume pondering after receiving time commands, as move will follow immediately
    if(!strcmp(command, "time"))    { sscanf(inBuf, "time %d", &timeLeft); continue; }
    if(!strcmp(command, "."))       { inBuf[0] = 0; return FALSE; } // ignore for now
    if(!root && !strcmp(command, "usermove")) {
      if(!strcmp(inBuf+9, ponderMoveText)) { // ponder hit
        inBuf[0] = 0; // eat away command, as we will process it here
        pondering = 0; SetTimeLimits(10*timeLeft + TimeUsed()); // turn into time-based search
        return FALSE; // do not abort
      }
    }
    return TRUE; // other commands (or ponder miss) abort search
  }
}

int TerminalCondition()
{
  if(pondering) { // only new input can cause abort
    if(InputWaiting()) return PeekAtInput(0); // but only abort if the input requires us
  } else { // when thinking only time can cause abort
    if(TimeUsed() > neverExceedLimit) return TRUE;
  }
  return FALSE;
}
