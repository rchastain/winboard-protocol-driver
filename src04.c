int rootScore;
MOVE rootMove;

void RootSearch()
{
  GenerateMoves();
  for(depth=1; STOP_CONDITION; depth++) { // iterative deepening loop
    bestScore = -INFINITY;
    for(ALL_MOVES) {
      MakeMove();
      score = -Search(...);
      UnMake();
      if(abortFlag) return;
      // handle score, etc.
      ...
    }
    // end of iteration
    ...
    rootMove = bestMove;
    rootScore = bestScore;
  } 
}