/********************************************************/
/* Example of a WinBoard-protocol driver, by H.G.Muller */
/********************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// four different constants, with values for WHITE and BLACK that suit your engine
#define WHITE    1
#define BLACK    2
#define NONE     0
#define ANALYZE  3

// some value that cannot occur as a valid move
#define INVALID 666

// some parameter of your engine
#define MAXMOVES 500  /* maximum game length  */
#define MAXPLY   60   /* maximum search depth */

#define OFF 0
#define ON  1

#ifndef WIN32
#  include <sys/ioctl.h>
#  include <sys/time.h>

#  define FALSE 0
#  define TRUE  1
#endif

#define DEFAULT_FEN "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"

typedef int MOVE;        // in this example moves are encoded as an int

int moveNr;              // part of game state
MOVE gameMove[MAXMOVES]; // holds the game history

// Some routines your engine should have to do the various essential things
int  MakeMove(int stm, MOVE move);      // performs move, and returns new side to move
void UnMake(MOVE move);                 // unmakes the move;
int  Setup(char *fen);                  // sets up the position from the given FEN, and returns the new side to move
void SetMemorySize(int n);              // if n is different from last time, resize all tables to make memory usage below n MB
char *MoveToText(MOVE move);            // converts the move from your internal format to text like e2e2, e1g1, a7a8q.
MOVE ParseMove(char *moveText);         // converts a long-algebraic text move to your internal move format
int  SearchBestMove(int stm, MOVE *move, MOVE *ponderMove);
void PonderUntilInput(int stm);         // Search current position for stm, deepening forever until there is input.

// Some global variables that control your engine's behavior
int ponder;
int randomize;
int postThinking;
int maxDepth;
int resign;         // engine-defined option
int contemptFactor; // likewise

int mps, timeControl, inc, timePerMove;                    // time-control parameters
int neverExceedLimit, noNewMoveLimit, noNewIterationLimit; // used by Search()
int timeLeft, startTime;
char inBuf[80], command[80], ponderMoveText[20];
char abortFlag, pondering;

int ReadClock()
{ // returns wall-clock time in msec
#ifdef WIN32
  return GetTickCount();
#else
  struct timeval t;

  gettimeofday(&t, NULL);

  return t.tv_sec*1000 + t.tv_usec/1000;

#endif
}

int InputWaiting()
{
#ifdef WIN32
  DWORD cnt;
  static HANDLE hInp = NULL;
  if(hInp == NULL) hInp = GetStdHandle(STD_INPUT_HANDLE);
  return !PeekNamedPipe(hInp, NULL, 0, NULL, &cnt, NULL) || cnt > 0;
#else
  static fd_set rset;
  static struct timeval timeout = {0, 0};
  FD_ZERO(&rset);
  FD_SET(0, &rset);
  if(select(1, &rset, NULL, NULL, &timeout) < 0) printf("error X\n");
  if (FD_ISSET(0, &rset))   return 1;
#endif
  return 0;
}

int TimeUsed()
{
  return ReadClock() - startTime;
}

#define GUESSEDLENGTH 40

void SetTimeLimits(int msecLeft)
{
  int movesToGo = mps - moveNr/2;

  if(timePerMove > 0) movesToGo = 1;           // in maximum-time-per-move mode, the time left is always for one move
  else if(mps == 0) movesToGo = GUESSEDLENGTH; // in sudden-death, we have to guess how many moves we still must do
  else while(movesToGo <= 0) movesToGo += mps; // calculate moves before next time control

  msecLeft -= 20; // keep absolute safety margin of 20 msec

  neverExceedLimit    =  10*msecLeft / (movesToGo + 9);
  noNewMoveLimit      = 1.5*msecLeft / (movesToGo + 0.5);
  noNewIterationLimit = 0.5*msecLeft / movesToGo;
}

void ReadLine()
{ // read one line of input
  int i, c;
  for(i = 0; (inBuf[i] = c = getchar()) != '\n'; i++) if(c == EOF || i>77) exit(0);
  inBuf[i+1] = 0;
}

int PeekAtInput(int root)
{
  while(1) {
    ReadLine(inBuf);
    sscanf(inBuf, "%s", command);
    if(!strcmp(command, "otim"))    { continue; } // do not resume pondering after receiving time commands, as move will follow immediately
    if(!strcmp(command, "time"))    { sscanf(inBuf, "time %d", &timeLeft); continue; }
    if(!strcmp(command, "."))       { inBuf[0] = 0; return FALSE; } // ignore for now
    if(!root && !strcmp(command, "usermove")) {
      if(!strcmp(inBuf+9, ponderMoveText)) { // ponder hit
        inBuf[0] = 0; // eat away command, as we will process it here
        pondering = 0; SetTimeLimits(10*timeLeft + TimeUsed()); // turn into time-based search
        return FALSE; // do not abort
      }
    }
    return TRUE; // other commands (or ponder miss) abort search
  }
}

int TerminalCondition()
{
  if(pondering) { // only new input can cause abort
    if(InputWaiting()) return PeekAtInput(0); // but only abort if the input requires us
  } else { // when thinking only time can cause abort
    if(TimeUsed() > neverExceedLimit) return TRUE;
  }
  return FALSE;
}

#ifdef EXAMPLE
// these are only examples for where and how you should test for time-control enforcement in your search

int rootScore;
MOVE rootMove;

int Search()
{
  nodeCount++;
  if((nodeCount & 0x3FF) == 0 && TerminalCondition()) abortFlag = TRUE; // test if we should abort (not too often)
  ...
    for(ALL_MOVES) {
      MakeMove();
      score = -Search(...);
      UnMake();
      if(abortFlag) return 0; // here we put an abort to make the search unwind
      ...
    }
  }
}

#define MARGIN 25

void RootSearch()
{
  int depth, score, bestScore; MOVE rootMove
  startTime = ReadClock();
  GenerateMoves();
  for(depth=1; depth<=maxDepth && (pondering || TimeUsed()<noNewIterationLimit); depth++) { // iterative deepening loop
    bestScore = -INFINITY;
    for(ALL_MOVES) {
      MakeMove();
      score = -Search(...);
      UnMake();
      if(abortFlag) return;
      // handle score, etc.
      ...
      if(!pondering && TimeUsed() > noNewMoveLimit && bestScore > rootScore - MARGIN) break; // extra time for fail low
    }
    // end of iteration
    ...
    rootMove = bestMove;
    rootScore = bestScore;
  }
}
#endif

int TakeBack(int n)
{ // reset the game and then replay it to the desired point
  int last, stm;
  stm = Setup(NULL); // assumed to remember argument it was last called with
  last = moveNr - n; if(last < 0) last = 0;
  for(moveNr=0; moveNr<last; moveNr++) stm = MakeMove(stm, gameMove[moveNr]);
  return stm;
}

void PrintResult(int stm, int score)
{
  if(score == 0) printf("1/2-1/2\n");
  if(score > 0 && stm == WHITE || score < 0 && stm == BLACK) printf("1-0\n");
  else printf("0-1\n");
}

#define OPPONENT(x) WHITE + BLACK - (x)

int main()
{
  int stm=WHITE;                           // side to move
  int engineSide=NONE;                     // side played by engine
  MOVE move, ponderMove;
  int score;

  while(1) { // infinite loop

    fflush(stdout); // make sure everything is printed before we do something that might take time
    inBuf[0] = 0; // empty input buffer, so we can detect if something appears in it

    pondering = (ponder && engineSide == OPPONENT(stm) && moveNr != 0); // calculate if we are expected to ponder
    if(engineSide == stm || pondering && ponderMove != INVALID) {
      MOVE newPonderMove; int oldStm = stm;
      if(pondering) { // we are apparently pondering on a speculative move
        sprintf(ponderMoveText, "%s\n", MoveToText(ponderMove)); // remember ponder move as text (for hit detection)
        stm = MakeMove(stm, ponderMove);
        gameMove[moveNr++] = ponderMove;  // remember game
      } else SetTimeLimits(10*timeLeft);
      score = SearchBestMove(stm, &move, &newPonderMove);
      if(pondering) { // pondering was aborted because of miss or other command
        UnMake(ponderMove); stm = oldStm; moveNr--;
        pondering = FALSE;
      } else if(move == INVALID) {  // game apparently ended
        engineSide = NONE;          // so stop playing
        PrintResult(stm, score);
      } else {
        stm = MakeMove(stm, move);  // assumes MakeMove returns new side to move
        gameMove[moveNr++] = move;  // remember game
        printf("move %s\n", MoveToText(move));
        ponderMove = newPonderMove;
        continue; // start pondering (if needed)
      }
    } else if(engineSide == ANALYZE || pondering) { // this catches pondering when we have no move
      MOVE dummy;
      pondering = TRUE;        // in case we must analyze
      ponderMoveText[0] = 0;   // make sure we will never detect a ponder hit
      SearchBestMove(stm, &dummy, &dummy);
      pondering = FALSE;
    }

    fflush(stdout); // make sure everything is printed before we do something that might take time
    // the previous calls to SeachBestMove() could have left a command that interrupted it in inBuf !
    if(inBuf[0] == 0) PeekAtInput(1); // handles time, otim

    // recognize the command, and execute it
    if(!strcmp(command, "quit"))    { break; } // breaks out of infinite loop
    if(!strcmp(command, "force"))   { engineSide = NONE;    continue; }
    if(!strcmp(command, "analyze")) { engineSide = ANALYZE; continue; }
    if(!strcmp(command, "exit"))    { engineSide = NONE;    continue; }
    if(!strcmp(command, "level"))   {
      int min, sec=0;
      sscanf(inBuf, "level %d %d %d", &mps, &min, &inc) == 3 ||  // if this does not work, it must be min:sec format
      sscanf(inBuf, "level %d %d:%d %d", &mps, &min, &sec, &inc);
      timeControl = 60*min + sec; timePerMove = -1;
      continue;
    }
    if(!strcmp(command, "protover")){
      printf("feature ping=1 setboard=1 colors=0 usermove=1 memory=1 debug=1");
      printf("feature option=\"Resign -check 0\"");           // example of an engine-defined option
      printf("feature option=\"Contempt -spin 0 -200 200\""); // and another one
      printf("feature done=1");
      continue;
    }
    if(!strcmp(command, "option")) { // setting of engine-define option; find out which
      if(sscanf(inBuf+7, "Resign=%d",   &resign)         == 1) continue;
      if(sscanf(inBuf+7, "Contempt=%d", &contemptFactor) == 1) continue;
      continue;
    }
    if(!strcmp(command, "sd"))      { sscanf(inBuf, "sd %d", &maxDepth);    continue; }
    if(!strcmp(command, "st"))      { sscanf(inBuf, "st %d", &timePerMove); continue; }
    if(!strcmp(command, "memory"))  { SetMemorySize(atoi(inBuf+7)); continue; }
    if(!strcmp(command, "ping"))    { printf("pong%s", inBuf+4); continue; }
    if(!strcmp(command, "easy"))    { ponder = OFF; continue; }
    if(!strcmp(command, "hard"))    { ponder = ON;  continue; }
    if(!strcmp(command, "go"))      { engineSide = stm;  continue; }
    if(!strcmp(command, "post"))    { postThinking = ON; continue; }
    if(!strcmp(command, "nopost"))  { postThinking = OFF;continue; }
    if(!strcmp(command, "random"))  { randomize = ON;    continue; }
    if(!strcmp(command, "hint"))    { if(ponderMove != INVALID) printf("Hint: %s\n", MoveToText(ponderMove)); continue; }
//  if(!strcmp(command, ""))        { sscanf(inBuf, " %d", &); continue; }
    // ignored commands:
    if(!strcmp(command, "book"))    { continue; }
    if(!strcmp(command, "xboard"))  { continue; }
    if(!strcmp(command, "computer")){ continue; }
    if(!strcmp(command, "name"))    { continue; }
    if(!strcmp(command, "ics"))     { continue; }
    if(!strcmp(command, "accepted")){ continue; }
    if(!strcmp(command, "rejected")){ continue; }
    if(!strcmp(command, "variant")) { continue; }
    if(!strcmp(command, ""))  {  continue; }
    // now process commands that do alter the position, and thus invalidate the ponder move
    ponderMove = INVALID;
    if(!strcmp(command, "new"))     { engineSide = BLACK; stm = Setup(DEFAULT_FEN); maxDepth = MAXPLY; randomize = OFF; continue; }
    if(!strcmp(command, "setboard")){ engineSide = NONE;  stm = Setup(inBuf+9); continue; }
    if(!strcmp(command, "undo"))    { stm = TakeBack(1); continue; }
    if(!strcmp(command, "remove"))  { stm = TakeBack(2); continue; }
    if(!strcmp(command, "usermove")){
      int move = ParseMove(inBuf+9);
      if(move == INVALID) printf("Illegal move\n");
      else {
        stm = MakeMove(stm, move);
        gameMove[moveNr++] = move;  // remember game
      }
      continue;
    }
    printf("Error: unknown command\n");
  }
  return 0;
}

int  MakeMove(int stm, MOVE move){return 0;}
void UnMake(MOVE move){}
int  Setup(char *fen){return 0;}
void SetMemorySize(int n){}
char *MoveToText(MOVE move){return NULL;}
MOVE ParseMove(char *moveText){return 0;}
int  SearchBestMove(int stm, MOVE *move, MOVE *ponderMove){return 0;}
void PonderUntilInput(int stm){}
