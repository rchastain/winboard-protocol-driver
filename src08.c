while(1) {
  if( ENGINE_ON_MOVE || PONDER_ON_MOVE ) {
    if( PONDER_ON_MOVE ) MAKE_PONDER_MOVE;
    THINK;
    if( INPUT_LEFT ) UNMAKE_PONDER_MOVE; // ponder search was interrupted
    else {
      MAKE_MOVE;
      continue;
    }
  } else
  PONDER_OR_ANALYZE;
  if( ! INPUT_LEFT ) READ_INPUT_LINE; // blocking input
  HANDLE_INPUT_COMMAND;
}