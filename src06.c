int InputWaiting()
{
#ifdef WIN32
  DWORD cnt;
  static HANDLE hInp = NULL;
  if(hInp == NULL) hInp = GetStdHandle(STD_INPUT_HANDLE);
  return !PeekNamedPipe(hInp, NULL, 0, NULL, &cnt, NULL) || cnt > 0;
#else
  static fd_set rset;
  static struct timeval timeout = {0, 0};
  FD_ZERO(&rset);
  FD_SET(0, &rset);
  if(select(1, &rset, NULL, NULL, &timeout) < 0) printf("error X\n");
  if (FD_ISSET(0, &rset))   return 1;
#endif
  return 0;
}