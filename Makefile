SOURCES := $(wildcard *.c)
TARGETS := $(SOURCES:%.c=%)

all: $(TARGETS)

%: %.c
	gcc -o $@ $<
